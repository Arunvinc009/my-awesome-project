import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArunvincComponent } from './arunvinc.component';

describe('ArunvincComponent', () => {
  let component: ArunvincComponent;
  let fixture: ComponentFixture<ArunvincComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArunvincComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArunvincComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
