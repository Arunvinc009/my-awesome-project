import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ArunComponent } from './arun/arun.component';
import { ArunvincComponent } from './arunvinc/arunvinc.component';

const routes: Routes = [
  {
    path : '',
    component: AppComponent,
    
      },
      {
    path : 'arun', 
      component : ArunComponent,
     
   children :[
    {path :'**', redirectTo :'arunvinc'},
   { path : 'arunvinc',
    component: ArunvincComponent,}

   ]
    },
      {
        path : 'arunvinc',
        component: ArunvincComponent,
        
          },
      

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
